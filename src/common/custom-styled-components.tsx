// styled-components.ts
import * as styledComponents from 'styled-components';
import { ThemedStyledComponentsModule } from 'styled-components';

import { IFitUnityTheme } from './../ThemeInterfaces/IFitUnityTheme';

const {
  default: styled,
  css,
  injectGlobal,
  keyframes,
  ThemeProvider
} = styledComponents as ThemedStyledComponentsModule<IFitUnityTheme>;

export { css, injectGlobal, keyframes, ThemeProvider };
export default styled;