// https://stackoverflow.com/questions/47077210/using-styled-components-with-props-and-typescript
// import * as React from 'react';
import { ThemedStyledFunction } from 'styled-components';

const withProps = <U>() => <P, T, O>(fn: ThemedStyledFunction<P, T, O>) =>
    fn as ThemedStyledFunction<P & U, T, O & U>;

export { withProps };