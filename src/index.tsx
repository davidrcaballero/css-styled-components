import * as React from 'react';
import * as ReactDOM from 'react-dom';
import FitUnityApp from './FitUnityApp';
import registerServiceWorker from './registerServiceWorker';
// import './index.css'; // could add global styles here, but probably a bad idea to mix approaches
import { ThemeProvider, injectGlobal } from './Common/custom-styled-components';

import ThemeBuilder from './ThemeBuilder/ThemeBuilder';
import { IFitUnityTheme } from './ThemeInterfaces/IFitUnityTheme';
import FontFamilies from './Enums/FontFamilies';
import BorderRegion from './Enums/BorderRegion';

// injectGlobal: used to add global styles. Same as using index.css
// tslint:disable-next-line:no-unused-expression
injectGlobal`
  html {
    box-sizing: border-box;
    font-size: 62.5%;
    font-family: 'Roboto', sans-serif;
  }
  body {
    margin: 0;
    padding: 0;
  }
`;

// ThemeBuilder.createTheme justs deep merges an object - nothing special happening here 
const selectedTheme = ThemeBuilder.createTheme(getTheme(3));

ReactDOM.render(
  <ThemeProvider theme={selectedTheme}>
    <FitUnityApp />
  </ThemeProvider>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();

// Only for PoC used to quickly change themes.
function getTheme(id?: number): IFitUnityTheme {
  let theme = { id: 0 };

  const theme1: IFitUnityTheme = {
    id: 1,
    ControlLabel: {
      fontFamily: FontFamilies.Risque
    }, 
    SingleValueInput: {
      fontFamily: FontFamilies.Risque,
      unitFontFamily: FontFamilies.IndieFlower,
      textColor: 'green',
      placeholderColor: 'rgba(0,0,0,0.4)',
      unitColor: 'green',
      border: {
        width: '4px',
        region: BorderRegion.Bottom,
        radius: '10px',
        focusColor: 'rgba(43, 76, 210, 0.66)',
        blurColor: 'rgba(49, 168, 229, 0.2)',
      },
    }
  };

  const theme2: IFitUnityTheme = {
    id: 2,
    ControlLabel: {
      fontFamily: FontFamilies.SupermercadoOne
    },
    SingleValueInput: {
      backgroundColor: 'rgba(255, 165, 0, 0.5)',
      textColor: '#e68780',
      placeholderColor: 'white',
      unitColor: '#fff0ef',
      border: {
        width: '4px',
        region: BorderRegion.Bottom,
        radius: '16px',
        focusColor: 'yellow',
        blurColor: 'rgba(0,0,0,0.2)',
        style: 'dotted'
      },
    }
  };

  const theme3: IFitUnityTheme = {
    id: 3,
    ControlLabel: {
      fontFamily: FontFamilies.Montserrat
    },
    SingleValueInput: {
      backgroundColor: 'rgba(233,0,0,0.2)',
      border: {
        width: '5px',
        region: BorderRegion.Full,
        focusColor: 'blue',
        blurColor: 'rgba(0,0,0,0.2)',
        style: 'dotted',
        radius: '40px'
      }
    },
    SingleValueControl: {
      backgroundColor: 'lightpink'
    }
  };

  const themes: Array<IFitUnityTheme> = [theme1, theme2, theme3];

  if (!id || id > themes.length) { return theme; }

  themes.forEach(t => {
    if (t.id === id) { theme = t; }
  });

  return theme;
}