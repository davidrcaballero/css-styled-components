import * as React from 'react';
import SingleValueControl from './Components/SingleValueControl/SingleValueControl';

export interface SingleValueControlProps {
  id: string;
  controlLabel: string;
  unitLabel: string;
  isFocused: boolean;
}

class FitUnityApp extends React.Component<{}, {}> {
  render() {
    let props: SingleValueControlProps = {
      id: 'height1-input',
      controlLabel: 'height',
      unitLabel: 'cm',
      isFocused: false,
    };

    return (
      <div>
        <SingleValueControl {...props} />
      </div>

    );
  }
}

export default FitUnityApp;
