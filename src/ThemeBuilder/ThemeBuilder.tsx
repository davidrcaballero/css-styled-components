import BaseFitUnityTheme from '../Themes/BaseTheme';
import { IFitUnityTheme } from './../ThemeInterfaces/IFitUnityTheme';
import { Utils } from '../Common/Utils';

class ThemeBuilder {
  
  static createTheme(theme: IFitUnityTheme) {
    return Utils.mergeDeep(BaseFitUnityTheme, theme);
  }
}

export default ThemeBuilder;