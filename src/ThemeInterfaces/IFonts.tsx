import { InterpolationValue } from 'styled-components';

// Not sure about the syncing that would need to happen between IFonts and the enum FontFamilies
export default interface IFonts {
  Roboto: InterpolationValue[];
  Arial: InterpolationValue[];
  SupermercadoOne: InterpolationValue[];
  Merriweather: InterpolationValue[];
  Montserrat: InterpolationValue[];
  OpenSans: InterpolationValue[];
  Risque: InterpolationValue[];
  VT323: InterpolationValue[];
  IndieFlower: InterpolationValue[];
}