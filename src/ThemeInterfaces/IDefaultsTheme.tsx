export default interface IDefaultsTheme {
  primaryColor?: string;
  primaryColorDisabled?: string;
  secondaryColorLight?: string;
  secondaryColorDark?: string;
  textPrimaryColor?: string;
  textPrimaryColorDisabled?: string;
  inputTextColor?: string;
  blurColor?: string; 
  errorColor: string;
  backgroundColor?: string;
  primaryFontFamily?: string;
  secondaryFontFamily?: string;
}