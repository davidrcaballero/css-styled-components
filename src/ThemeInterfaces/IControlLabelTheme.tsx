import FontFamilies from '../Enums/FontFamilies';

export default interface IControlLabelTheme {
  backgroundColor?: string | null;
  textColor?: string;
  fontFamily?: FontFamilies | null;
}