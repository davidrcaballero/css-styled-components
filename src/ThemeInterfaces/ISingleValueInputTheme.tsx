import FontFamilies from '../Enums/FontFamilies';
import BorderRegion from '../Enums/BorderRegion';

export default interface ISingleValueTheme {
  backgroundColor?: string | null;
  textColor?: string | null;
  placeholderColor?: string | null;
  unitColor?: string | null;
  fontFamily?: FontFamilies | null;
  unitFontFamily?: FontFamilies | null;
  border?: { // remove options
    width?: string;
    style?: string;
    focusColor?: string | null;
    blurColor?: string | null;
    region?: BorderRegion | null;
    radius?: string;
  };
}