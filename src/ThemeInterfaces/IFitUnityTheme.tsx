import {
  IControlLabelTheme,
  ISingleValueInputTheme,
  IDefaultsTheme,
  IControlIconTheme,
  ISingleValueControlTheme
} from './index';

export interface IFitUnityTheme {
  id: number;
  Defaults?: IDefaultsTheme;
  ControlIcon?: IControlIconTheme;
  SingleValueInput?: ISingleValueInputTheme;
  ControlLabel?: IControlLabelTheme;
  SingleValueControl?: ISingleValueControlTheme;
}