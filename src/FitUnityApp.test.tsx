import * as React from 'react';
import * as ReactDOM from 'react-dom';
import FitUnityApp from './FitUnityApp';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FitUnityApp />, div);
});
