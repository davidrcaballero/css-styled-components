import { css } from 'styled-components';
import IFonts from '../../ThemeInterfaces/IFonts';

const SingleValueInputFonts: IFonts = {
    Arial: css`
        font-family: Arial, sans-serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `,
    IndieFlower: css`
        font-family: 'Indie Flower', cursive;
        font-size: 3.9rem;
        padding: 0;
        width: 60px;
    `, 
    Roboto: css`
        font-family: Roboto, sans-serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `,
    SupermercadoOne: css`
        font-family: 'Supermercado One', cursive;
        font-size: 2.5rem;
        line-height: 3rem;
    `, 
    Merriweather: css`
        font-family: Merriweather, serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `,
    Montserrat: css`
        font-family: Montserrat, sans-serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `,
    OpenSans: css`
        font-family: 'Open Sans', sans-serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `, 
    Risque: css`
        font-family: 'Risque', cursive;
        font-size: 2.5rem;
        line-height: 3rem;
    `,
    VT323: css`
        font-family: 'VT323', monospace;
        font-size: 4rem;
        line-height: 3rem;
    `,
};

const SingleValueInputUnitFonts: IFonts = {
    Arial: css`
        font-family: Arial, sans-serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `,
    IndieFlower: css`
        font-family: 'Indie Flower', cursive;
        font-size: 2.5rem;
        line-height: 3rem;
    `, 
    Roboto: css`
        font-family: Roboto, sans-serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `,
    SupermercadoOne: css`
        font-family: 'Supermercado One', cursive;
        font-size: 2.0rem;
        line-height: 3rem;
    `, 
    Merriweather: css`
        font-family: Merriweather, serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `,
    Montserrat: css`
        font-family: Montserrat, sans-serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `,
    OpenSans: css`
        font-family: 'Open Sans', sans-serif;
        font-size: 2.5rem;
        line-height: 3rem;
    `, 
    Risque: css`
        font-family: 'Risque', cursive;
        font-size: 2.7rem;
        line-height: 3rem;
    `,
    VT323: css`
        font-family: 'VT323', monospace;
        font-size: 3.0rem;
        line-height: 3rem;
    `,
};

export {SingleValueInputFonts, SingleValueInputUnitFonts};