// import styled from '../../common/custom-styled-components'; // props include theme interface
import styled from 'styled-components';
import BorderRegion from '../../Enums/BorderRegion';
import { SingleValueInputFonts, SingleValueInputUnitFonts } from './SingleValueInputFonts';
// import { withProps } from './../../common/withProps'; // away to include typescript props

export const Wrapper: any = styled.div`
    position: relative;
    text-align: center;    
    background-color: ${props => wrapperBackgroundColor(props)};
    border-width: ${props => wrapperBorderWidth(props)};
    border-color: ${props => wrapperBorderColor(props)};
    border-style: ${props => props.theme.SingleValueInput.border.style};
    border-radius: ${ props => props.theme.SingleValueInput.border.radius};
    overflow: hidden;
`;

function wrapperBackgroundColor(props: any) {
  return props.theme.SingleValueInput.backgroundColor
    || props.theme.Defaults.backgroundColor;
}

function wrapperBorderWidth(props: any) {
  return isBorderFull(props) ? props.theme.SingleValueInput.border.width : 0;
}

function wrapperBorderColor(props: any) {
  const { Defaults, SingleValueInput } = props.theme;
  let borderColor = '';

  if (isBorderFull(props)) {
    if (props.isFocused) {
      borderColor = SingleValueInput.border.focusColor || Defaults.primaryColor;
    } else {
      borderColor = SingleValueInput.border.blurColor || Defaults.blur;
    }
  } else {
    borderColor = SingleValueInput.backgroundColor;
  }

  // (props.theme.SingleValueInput.border.focusColor || props.theme.Defaults.primaryColor)
  // : (props.theme.Defaults.blur || props.theme.SingleValueInput.border.blurColor);

  return borderColor;
}

export const Input: any = styled.input`
  height: auto;
  display: inline-block;
  font-size: 2.5rem;
  width: 46.1172px; // why 46.11px
  text-align: right;
  border: none;
  outline: none;
  color: ${ props => InputColor(props)};
  background-color: rgba(0,0,0,0);
  
  ${props => InputFont(props)}

  &::placeholder {
    color: ${ props => props.theme.SingleValueInput.placeholderColor
    || props.theme.Defaults.blurColor};
  }
  &:focus {
    &::placeholder {
      color: ${ props => props.theme.SingleValueInput.backgroundColor
    || props.theme.Defaults.backgroundColor}
    }
  }
`;

function InputColor(props: any) {
  const { SingleValueInput, Defaults } = props.theme;
  let inputColor = Defaults.blurColor;

  if (props.hasError) {
    inputColor = Defaults.errorColor;
  } else if (props.isValid) {
    inputColor = SingleValueInput.textColor || Defaults.primaryColor;
  }

  return inputColor;
}

function InputFont(props: any) {
  return SingleValueInputFonts[props.theme.SingleValueInput.fontFamily]
    || props.theme.Defaults.primaryFontFamily;
}

export const Unit: any = styled.div`
  text-align: center;
  height: auto;
  width: auto;
  display: inline-block;
  color: ${ props => UnitColor(props)};
  font-size: 2.5rem;
  
  ${props => UnitFont(props)}
`;

function UnitColor(props: any) {
  const { SingleValueInput, Defaults } = props.theme;
  let inputColor = Defaults.blurColor;

  if (props.hasError) {
    inputColor = Defaults.errorColor;
  } else if (props.isValid) {
    inputColor = SingleValueInput.unitColor || Defaults.primaryColor;
  }

  return inputColor;
}

function UnitFont(props: any) {
  return SingleValueInputUnitFonts[props.theme.SingleValueInput.unitFontFamily]
    || props.theme.Defaults.primaryFontFamily;
}

export const BorderBottom: any = styled.span`
  display: ${ props => BorderBottomDisplay(props)};
  height: ${ (props) => props.theme.SingleValueInput.border.width};
  width: auto;
  background-color: ${props => BorderBottomBackgroundColor(props)};
`;

function BorderBottomDisplay(props: any) {
  return props.theme.SingleValueInput.border.region === BorderRegion.Bottom ? 'block' : 'none';
}

function BorderBottomBackgroundColor(props: any) {
  return props.isFocused ?
    (props.theme.SingleValueInput.border.focusColor || props.theme.Defaults.primaryColor)
    : (props.theme.Defaults.blur || props.theme.SingleValueInput.border.blurColor);
}

// example using withProps
// export const BorderBottom = withProps<BorderBottomProps>()(styled.span) `
//   display: ${props => props.theme.SingleValueInput.border.region === 'bottom' ? 'block' : 'none'};
//   height: ${(props) => props.theme.SingleValueInput.border.width};
//   width: auto;
//   background-color: ${props => {
//     console.log(props);
//     return props.isFocused
//       ? props.theme.SingleValueInput.border.focusColor
//       : props.theme.SingleValueInput.border.blurColor;
//   }};
// `;

function isBorderFull(props: any) {
  return props.theme.SingleValueInput.border.region === BorderRegion.Full;
}