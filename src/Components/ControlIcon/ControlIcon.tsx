import * as React from 'react';
const heightIcon = require('./heightIcon.svg') as string; // plan to change would be dynamic

import styled from 'styled-components';

const Container: any = styled.div`
  float: ${(props: any) => props.isHorizontal ? 'left' : 'none'};
  padding: .5rem;
  text-align: center;
  background-color: ${props => props.theme.ControlIcon.backgroundColor
    || props.theme.Defaults.background};  
`;

const Image: any = styled.img`
/* animation: App-logo-spin infinite 20s linear; */
    height: 3.7rem;
    width: 3.7rem;
    ${(props: any) => props.isFocused && 'background: red;'}
`;

const ControlIcon = (props: any) => {
  return (
    <Container isHorizontal={props.isHorizontal}>
      <Image isFocused={props.isFocused} src={heightIcon} />
    </Container>
  );
};

export default ControlIcon;