import { css } from 'styled-components';
import IFonts from '../../ThemeInterfaces/IFonts';

const ControlLabelFonts: IFonts = {
    Roboto: css`
        font-family: Roboto, sans-serif;
        font-size: 1.6rem;
        line-height: 3rem;
    `,
    Arial: css`
        font-family: Arial, sans-serif;
        font-size: 1.6rem;
        line-height: 3rem;
    `,
    SupermercadoOne: css`
        font-family: 'Supermercado One', cursive;
        font-size: 2.5rem;
        line-height: 3rem;
    `, 
    Merriweather: css`
        font-family: Merriweather, serif;
        font-size: 1.6rem;
        line-height: 3rem;
    `,
    Montserrat: css`
        font-family: Montserrat, sans-serif;
        font-size: 1.6rem;
        line-height: 3rem;
    `,
    OpenSans: css`
        font-family: 'Open Sans', sans-serif;
        font-size: 2.0rem;
        line-height: 3rem;
    `, 
    Risque: css`
        font-family: 'Risque', cursive;
        font-size: 1.6rem;
        line-height: 3rem;
    `,
    VT323: css`
        font-family: 'VT323', monospace;
        font-size: 2.0rem;
        line-height: 3rem;
    `, 
    IndieFlower: css`
        font-family: 'Indie Flower', cursive;
        font-size: 2.0rem;
        line-height: 3rem;
    `, 
};

export default ControlLabelFonts;