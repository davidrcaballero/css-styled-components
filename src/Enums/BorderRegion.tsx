enum BorderRegion {
    Full = 'Full',
    Bottom = 'Bottom',
    None = 'None'
}

export default BorderRegion;