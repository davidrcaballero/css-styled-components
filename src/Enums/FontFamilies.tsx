enum FontFamilies {
    Roboto = 'Roboto',
    Arial = 'Arial',
    SupermercadoOne = 'SupermercadoOne',
    Merriweather = 'Merriweather',
    Montserrat = 'Montserrat',
    OpenSans = 'OpenSans',
    Risque = 'Risque',
    VT323 = 'VT323',
    IndieFlower = 'IndieFlower'
}

export default FontFamilies;