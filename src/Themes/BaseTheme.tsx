import { IFitUnityTheme } from '../ThemeInterfaces/IFitUnityTheme';

const BaseFitUnityTheme: IFitUnityTheme = {
    id: 0, // probably better to use meaningful theme names?
    Defaults: {
        primaryColor: '#83209C',
        primaryColorDisabled: '#BDBDBD',
        secondaryColorLight: '#BDBDBD',
        secondaryColorDark: '#616161',
        textPrimaryColor: '#ffffff',
        textPrimaryColorDisabled: '#ffffff',
        inputTextColor: '#000000',
        blurColor: '#b7b7b7',
        errorColor: '#f44336',
        backgroundColor: '#ffffff',
        primaryFontFamily: 'Roboto, sans-serif',
        secondaryFontFamily: 'Arial, sans-serif'
    },
    ControlIcon: {
        backgroundColor: null,
    },
    ControlLabel: {
        fontFamily: null,
        backgroundColor: null,
    },
    SingleValueInput: {
        backgroundColor: null,
        fontFamily: null,
        textColor: null,
        placeholderColor: null,
        unitColor: null,
        unitFontFamily: null,
        border: { 
            width: '2px',
            style: 'solid',
            focusColor: null,
            blurColor: '#b7b7b7',
            region: null,
            radius: '0px'
        },
        // text: {
        //     font: 'Roboto',
        //     color: 'black'
        // }
    },
    SingleValueControl: {
        backgroundColor: null
    }
};

export default BaseFitUnityTheme;