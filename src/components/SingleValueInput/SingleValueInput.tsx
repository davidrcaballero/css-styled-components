import * as React from 'react';
import { Wrapper, Input, Unit, BorderBottom } from './SingleValueInputStyle';

interface SingleInputThemedProps {
  id: string;
  unitLabel: string;
  onInputFocus: Function;
  onInputBlur: Function;
  isFocused: boolean;
}

class SingleValueInput extends React.Component<SingleInputThemedProps> {
  inputNode: HTMLInputElement;

  state = {
    hasError: false,
    isValid: false
  };

  handleClick = () => {
    this.inputNode.focus();
  }

  handleChange = (e: any) => {
    // const value = this.inputNode.value;
    const value = +this.inputNode.value;

    if (value !== value || (value < 119 || value > 200)) {
        this.setState({hasError: true, isValid: false});
      } else {
        this.setState({hasError: false, isValid: true});
      }
  }

  handleInputFocus = (e: any) => {
    this.props.onInputFocus(e);
  }

  handleBlur = (e: any) => {
    this.props.onInputBlur(e);
  }

  SetupInputInnerRef = (node: HTMLInputElement) => {
    this.inputNode = node;
  }

  render() {
    const { id, unitLabel } = this.props;
    return (
      <Wrapper isFocused={this.props.isFocused} onClick={this.handleClick}>
        <Input
          type="text"
          id={id}
          placeholder="0"
          onFocus={this.handleInputFocus}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          innerRef={this.SetupInputInnerRef}
          isFocused={this.props.isFocused}
          hasError={this.state.hasError}
          isValid={this.state.isValid}
        />
        {unitLabel && <Unit hasError={this.state.hasError} isValid={this.state.isValid}>
                          {unitLabel}
                      </Unit>}
        <BorderBottom isFocused={this.props.isFocused} />
      </Wrapper>
    );
  }
}

export default SingleValueInput;