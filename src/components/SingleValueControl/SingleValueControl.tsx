import * as React from 'react';
import ControlLabel from '../../Components/ControlLabel/ControlLabel';
import ControlIcon from '../../Components/ControlIcon/ControlIcon';
import SingleValueInput from '../../Components/SingleValueInput/SingleValueInput';

import styled, { css } from 'styled-components';

export interface SingleValueControlProps {
    id: string;
    controlLabel: string;
    unitLabel: string;
    isFocused: boolean;
}

// grid components and styles would be separate file/project
const clearFix = css`&:before { 
    content: ""; 
    display: table; 
    clear: both; }`;

const Wrapper: any = styled.div`
    width: 36%;
    margin: 50px auto; 
    background-color: ${props => props.theme.SingleValueControl.backgroundColor || props.theme.Defaults.background }
`;

const Row: any = styled.section`
${(props: any) => {
        if (props.isHorizontal) {
            return clearFix;
        }
        return '';
    }}`;

const Column: any = styled.div`
${(props: any) => props.isHorizontal && 'float: left;'}
    overflow:hidden;
`;

class SingleValueControl extends React.Component<SingleValueControlProps, {}> {
    state = {
        isFocused: this.props.isFocused
    };

    handleInputFocus = (e: any) => {
        this.setState({ isFocused: true });
    }
    handleInputBlur = (e: any) => {
        this.setState({ isFocused: false });
    }

    render() {
        let isHorizontal = false;
        return (
            <Wrapper>
                <Row isHorizontal={isHorizontal}>
                    <Column isHorizontal={isHorizontal}>
                        <ControlIcon isFocused={this.state.isFocused} />
                    </Column>
                    <Column isHorizontal={isHorizontal}>
                        <ControlLabel isFocused={this.state.isFocused} htmlFor={this.props.id}>
                            {this.props.controlLabel}
                        </ControlLabel>
                    </Column>
                    <Column isHorizontal={isHorizontal}>
                        <SingleValueInput
                            id={this.props.id}
                            unitLabel={this.props.unitLabel}
                            onInputFocus={this.handleInputFocus}
                            onInputBlur={this.handleInputBlur}
                            isFocused={this.state.isFocused}
                        />
                    </Column>
                </Row>
            </Wrapper>
        );
    }
}

export default SingleValueControl;