import styled from 'styled-components';
import ControlLabelFonts from './ControlLabelFonts';

export const StyledControlLabel = styled.label`
    text-transform: uppercase;
    font-weight: 500;
    font-size: 1.6rem;
    height: 3rem;
    display: block;
    /* display: flex; */
    /* flex-direction: column-reverse; */

    text-align: center;
    padding-bottom: .5rem;
    /* width: 100%; */
    color: #84818B;
    background-color: ${props => StyledControlLabelBackgroundColor(props)};

    ${props => getFont(props)}
`;

function getFont(props: any) {
    return ControlLabelFonts[props.theme.ControlLabel.fontFamily]
        || props.theme.Defaults.primaryFontFamily;
}

function StyledControlLabelBackgroundColor(props: any) {
    const { Defaults, ControlLabel } = props.theme;
    return ControlLabel.backgroundColor || Defaults.background;
}