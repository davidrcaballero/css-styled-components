import * as React from 'react';
import { StyledControlLabel } from './ControlLabelStyle';

interface IControlLabel {
    htmlFor: string;
    children?: string;
    isFocused: boolean;
}

const ControlLabel = (props: IControlLabel) => {
    return <StyledControlLabel {...props}>{props.children}</StyledControlLabel>;
};

export default ControlLabel;